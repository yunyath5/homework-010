import React,{useState,useEffect} from "react";
import { Col, Container, Row,Form,Button } from "react-bootstrap";
import { useParams } from "react-router";
import { fetchArticleById, postArticle, updateArticleById, uploadImage } from "../services/article_service";
import { fetchCategory } from "../services/category_service";

function Post() {


  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
  const [imageFile, setImageFile] = useState(null)
  const [categoryId, setCategoryId] = useState(null)

  const [category, setCategory] = useState([])

  const {id} = useParams()

  useEffect(() => {

    if(id){
      fetchArticleById(id).then(article=>{
        setTitle(article.title)
        setDescription(article.description)
        setImageURL(article.image)
        setCategoryId(article.category._id)
      })
    }

    fetchCategory().then(category=>{
      setCategoryId(category[0])
      setCategory(category)
    })

  }, [])


  const onAdd = async(e)=>{
      e.preventDefault()
      let article = {
          title,description,category:categoryId
      }
      

      if(imageFile){
         let url = await uploadImage(imageFile)
         article.image = url
      }
      postArticle(article).then(message=>alert(message))
  }

  const onUpdate = async(e)=>{
    e.preventDefault()
    let article = {
        title,description,category:categoryId
    }
    if(imageFile){
       let url = await uploadImage(imageFile)
       article.image = url
    }
    updateArticleById(id,article).then(message=>alert(message))
}

  return (
    <Container>
      <h1 className="my-2">{id?"Update Article":"Add Article"}</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label>Title</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Title" 
                value={title}
                onChange={(e)=>setTitle(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>Category</Form.Label>
              <Form.Control 
                as='select' 
                aria-label="Choosse Category"
                onChange={(e)=>setCategoryId(e.target.value)}
                >
                {
                  category.map(category=>
                    <option key={category._id} value={category._id} selected={category._id === categoryId}>{category.name}</option>
                  )
                }
              </Form.Control>
              
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control 
                as="textarea" 
                rows={4} 
                placeholder="Description" 
                value={description}
                onChange={(e)=>setDescription(e.target.value)}
              />
              
            </Form.Group>
            <Button 
                variant="primary" 
                type="submit"
                onClick={id? onUpdate:onAdd}
            >
              {id?'Save':'Submit'}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Post;
